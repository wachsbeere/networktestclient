package com.example.networktestclient;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SimpleTCPClient {

    public void simpleTestConnect() {
        try {
            // 1.创建TCP客户端Socket服务
            Socket client = new Socket();
            // 2.与服务端进行连接
            InetSocketAddress address = new InetSocketAddress("10.0.0.22", 1234);
            client.connect(address);
            // 3.连接成功后获取客户端Socket输出流
            OutputStream outputStream = client.getOutputStream();
            // 4.通过输出流往服务端写入数据
            outputStream.write("hello server".getBytes());
            // 5.关闭流
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
