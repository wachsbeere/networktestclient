package com.example.networktestclient;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public abstract class SocketTransceiver implements Runnable {

    protected Socket socket;
    protected InetAddress addr;

    protected DataInputStream dataIn;
    protected DataOutputStream dataOut;

//    protected InputStream inputStream;
//    protected OutputStream outputStream;

    private boolean runFlag;

    public SocketTransceiver(Socket socket) {
        this.socket = socket;
        this.addr = socket.getInetAddress();
    }

    public void start() {
        runFlag = true;
        new Thread(this).start();
    }

    // 断开连接
    public void stop() {
        runFlag = false;
        try {
            socket.shutdownInput();
            dataIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 向Server发送消息
    public boolean send(String s) {
        if (dataOut != null) {
            try {
                dataOut.writeUTF(s);
                dataOut.flush();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void run() {
        try {
            dataIn = new DataInputStream(this.socket.getInputStream());
            dataOut = new DataOutputStream(this.socket.getOutputStream());

//            inputStream = this.socket.getInputStream();
//            outputStream = this.socket.getOutputStream();

//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//            String line = null;
//            while ((line = bufferedReader.readLine()) != null) {
//                System.out.println(line);
//            }

        } catch (IOException e) {
            e.printStackTrace();
            runFlag = false;
        }
        while (runFlag) {
            try {
                final String s = dataIn.readUTF();
                this.onReceive(addr, s);
            } catch (IOException e) {
                runFlag = false;
            }
        }
        // 断开连接
        try {
            dataIn.close();
            dataOut.close();
            socket.close();
            dataIn = null;
            dataOut = null;
            socket = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.onDisconnect(addr);
    }

    public abstract void onReceive(InetAddress addr, String s);

    public abstract void onDisconnect(InetAddress addr);
}
