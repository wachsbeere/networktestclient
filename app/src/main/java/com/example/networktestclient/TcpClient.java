package com.example.networktestclient;

import android.os.CountDownTimer;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public abstract class TcpClient implements Runnable {

    private int port;
    private String hostIP;
    private boolean connect = false;
    private SocketTransceiver transceiver;
    private boolean isTestConnectBroke3 = false;

    private boolean testFlag = false;

    public void connect(String hostIP, int port, boolean isTestConnectBroke3) {
        this.hostIP = hostIP;
        this.port = port;
        this.isTestConnectBroke3 = isTestConnectBroke3;
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
//            Socket socket = new Socket(hostIP, port);

            Socket socket = new Socket();
            InetSocketAddress address = new InetSocketAddress(hostIP, port);


            // test
            if (isTestConnectBroke3) {
                socket.connect(address, 1);
            } else {
                socket.connect(address);
            }

            transceiver = new SocketTransceiver(socket) {
                @Override
                public void onReceive(InetAddress addr, String s) {
                    TcpClient.this.onReceive(this, s);
                }

                @Override
                public void onDisconnect(InetAddress addr) {
                    connect = false;
                    TcpClient.this.onDisconnect(this);
                }
            };
            transceiver.start();
            connect = true;
            this.onConnect(transceiver);
        } catch (Exception e) {
            e.printStackTrace();
            this.onConnectFailed();
        }
    }

    public void disconnect() {
        if (transceiver != null) {
            transceiver.stop();
            transceiver = null;
        }
    }

    public boolean isConnected() {
        return connect;
    }

    public SocketTransceiver getTransceiver() {
        return isConnected() ? transceiver : null;
    }

    public abstract void onConnect(SocketTransceiver transceiver);

    public abstract void onConnectFailed();

    public abstract void onReceive(SocketTransceiver transceiver, String s);

    public abstract void onDisconnect(SocketTransceiver transceiver);
}
